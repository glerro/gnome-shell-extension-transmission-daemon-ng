/* -*- Mode: js; indent-tabs-mode: nil; js-basic-offset: 4; tab-width: 4; -*- */
/*
 * This file is part of Transmission Daemon NG.
 * https://gitlab.gnome.org/glerro/gnome-shell-extension-transmission-daemon-ng
 *
 * prefs.js
 *
 * Copyright (c) 2023-2024 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Transmission Daemon NG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transmission Daemon NG is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Transmission Daemon NG. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2023-2024 Gianni Lerro <glerro@pm.me>
 */

'use strict';

import GObject from 'gi://GObject';
import Gio from 'gi://Gio';
import Adw from 'gi://Adw';

import {ExtensionPreferences, gettext as _} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';

const TEMPLATE_ADW = 'prefs_adw1.ui';

const TransmissionDaemonNGPrefsWidget = GObject.registerClass({
    GTypeName: 'TransmissionDaemonNGPrefsWidget',
    Template: import.meta.url.replace('prefs.js', TEMPLATE_ADW),
    InternalChildren: ['indicatorShowInStatus', 'numberOfTorrentShowInStatus',
        'speedUpDownIconShowInStatus', 'speedUpDownShowInStatus', 'hostname',
        'httpPortNumber', 'sslStatus', 'url', 'username', 'password'],
}, class TransmissionDaemonNGPrefsWidget extends Adw.PreferencesPage {
    constructor(settings, params = {}) {
        super(params);

        this._settings = settings;

        this._settings.bind('always-show', this._indicatorShowInStatus,
            'active', Gio.SettingsBindFlags.DEFAULT);

        this._settings.bind('stats-torrents', this._numberOfTorrentShowInStatus,
            'active', Gio.SettingsBindFlags.DEFAULT);

        this._settings.bind('stats-icons', this._speedUpDownIconShowInStatus,
            'active', Gio.SettingsBindFlags.DEFAULT);

        this._settings.bind('stats-numeric', this._speedUpDownShowInStatus,
            'active', Gio.SettingsBindFlags.DEFAULT);

        this._settings.bind('host', this._hostname,
            'text', Gio.SettingsBindFlags.DEFAULT);

        this._settings.bind('port', this._httpPortNumber,
            'value', Gio.SettingsBindFlags.DEFAULT);

        this._settings.bind('ssl', this._sslStatus,
            'active', Gio.SettingsBindFlags.DEFAULT);

        this._settings.bind('url', this._url,
            'text', Gio.SettingsBindFlags.DEFAULT);

        this._settings.bind('user', this._username,
            'text', Gio.SettingsBindFlags.DEFAULT);

        this._settings.bind('password', this._password,
            'text', Gio.SettingsBindFlags.DEFAULT);
    }
});

export default class TransmissionDaemonPreferences extends ExtensionPreferences {
    fillPreferencesWindow(window) {
        const page = new TransmissionDaemonNGPrefsWidget(this.getSettings());

        window.add(page);

        window.set_default_size(640, 770);
    }
}

