/* -*- Mode: js; indent-tabs-mode: nil; js-basic-offset: 4; tab-width: 4; -*- */
/*
 * This file is part of Transmission Daemon NG.
 * https://gitlab.gnome.org/glerro/gnome-shell-extension-transmission-daemon-ng
 *
 * extension.js
 *
 * Copyright (c) 2023-2024 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Transmission Daemon NG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transmission Daemon NG is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Transmission Daemon NG. If not, see <https://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 * Original Author: Jean-Philippe Braune - https://github.com/eonpatapon
 *                  Kevin James - https://github.com/TheKevJames
 * *****************************************************************************
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2023-2024 Gianni Lerro <glerro@pm.me>
 */

'use strict';

import Clutter from 'gi://Clutter';
import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';
import Shell from 'gi://Shell';
import Soup from 'gi://Soup';
import St from 'gi://St';

import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import * as PanelMenu from 'resource:///org/gnome/shell/ui/panelMenu.js';
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';

let upArrow = '';
let downArrow = '';
try {
    upArrow = decodeURIComponent(escape('↑')).toString();
    downArrow = decodeURIComponent(escape('↓')).toString();
} catch (e) {
    upArrow = '↑';
    downArrow = '↓';
}

const enabledIcon = 'utilities-transmission-daemon';
const errorIcon = 'utilities-transmission-daemon-error';
const connectingIcon = 'utilities-transmission-daemon-connecting';
const turtleIcon = 'utilities-transmission-daemon-turtle';

let TransmissionStatus = {
    STOPPED: 0,
    CHECK_WAIT: 1,
    CHECK: 2,
    DOWNLOAD_WAIT: 3,
    DOWNLOAD: 4,
    SEED_WAIT: 5,
    SEED: 6,
};

const TransmissionError = {
    NONE: 0,
    TRACKER_WARNING: 1,
    TRACKER_ERROR: 2,
    LOCAL_ERROR: 3,
};

const ErrorType = {
    NO_ERROR: 0,
    CONNECTION_ERROR: 1,
    AUTHENTICATION_ERROR: 2,
    CONNECTING: 3,
};

const StatusFilter = {
    ALL: 0,
    ACTIVE: 1,
    DOWNLOADING: 2,
    SEEDING: 3,
    PAUSED: 4,
    FINISHED: 5,
};

let StatusFilterLabels = { };

const TDAEMON_HOST_KEY = 'host';
const TDAEMON_PORT_KEY = 'port';
const TDAEMON_USER_KEY = 'user';
const TDAEMON_PASSWORD_KEY = 'password';
const TDAEMON_RPC_URL_KEY = 'url';
const TDAEMON_SSL_KEY = 'ssl';
const TDAEMON_STATS_NB_TORRENTS_KEY = 'stats-torrents';
const TDAEMON_STATS_ICONS_KEY = 'stats-icons';
const TDAEMON_STATS_NUMERIC_KEY = 'stats-numeric';
const TDAEMON_ALWAYS_SHOW_KEY = 'always-show';
const TDAEMON_LATEST_FILTER = 'latest-filter';
const TDAEMON_TORRENTS_DISPLAY = 'torrents-display';

const TorrentDisplayClass = {
    TransmissionTorrentMenu: 0,
    TransmissionTorrentMenuSmall: 1,
};

let transmissionDaemonMonitor;
let transmissionDaemonIndicator;

const TransmissionDaemonMonitor = class TransmissionDaemonMonitor {
    constructor(extension) {
        this._extensionName = extension.metadata.name;
        this._settings = extension.getSettings();

        this._url = '';
        this._session_id = false;
        this._torrents = false;
        this._stats = false;
        this._session = false;
        this._timers = {};
        this._interval = 10;

        this._httpSession = new Soup.Session();
        this._httpSession.set_proxy_resolver(Gio.ProxyResolver.get_default());
        this._httpSession.timeout = 10;

        this.updateURL();
        this.retrieveInfos();

        this._settings.connectObject('changed', () => {
            this.updateURL();
            this.retrieveInfos();
        }, this);
    }

    updateURL() {
        let host = this._settings.get_string(TDAEMON_HOST_KEY);
        let port = this._settings.get_int(TDAEMON_PORT_KEY);
        let rpcUrl = this._settings.get_string(TDAEMON_RPC_URL_KEY);
        let method = this._settings.get_boolean(TDAEMON_SSL_KEY) ? 'https' : 'http';

        if (method === 'https')
            port = 443;

        this._url = '%s://%s:%s%srpc'.format(method, host, port.toString(), rpcUrl);
    }

    authenticate(message, auth, retrying) {
        let user = this._settings.get_string(TDAEMON_USER_KEY);
        let password = this._settings.get_string(TDAEMON_PASSWORD_KEY);

        if (retrying) {
            transmissionDaemonIndicator.connectionError(
                ErrorType.AUTHENTICATION_ERROR,
                _('Authentication failed'));
            return false;
        }

        if (user && password) {
            auth.authenticate(user, password);
        } else {
            transmissionDaemonIndicator.connectionError(
                ErrorType.AUTHENTICATION_ERROR,
                _('Missing username or password'));
            return false;
        }

        // Returning TRUE means we have or *will* handle it.
        return true;
    }

    acceptCertificate(_message, _certificate, _tlserror) {
        // Here you can inspect @certificate or compare it against a trusted one
        // and you can see what is considered invalid by @tls_errors.

        // Returning TRUE trusts it anyway.
        return true;
    }

    changeInterval(interval) {
        this._interval = interval;
        for (let source in this._timers)
            GLib.source_remove(this._timers[source]);

        this.retrieveInfos();
    }

    sendPost(data, callback) {
        let message = Soup.Message.new_from_encoded_form('POST', this._url, JSON.stringify(data));

        message.connectObject('authenticate', this.authenticate.bind(this),
            'accept-certificate', this.acceptCertificate.bind(this), this);

        if (this._session_id)
            message.request_headers.append('X-Transmission-Session-Id', this._session_id);

        this._httpSession.send_and_read_async(message, GLib.PRIORITY_DEFAULT, null, callback.bind(this));
    }

    retrieveInfos() {
        this.retrieveStats();
        this.retrieveSession();
        this.retrieveList();
    }

    retrieveList() {
        let params = {
            method: 'torrent-get',
            arguments: {
                fields: [
                    'error', 'errorString', 'id', 'isFinished', 'leftUntilDone',
                    'name', 'peersGettingFromUs', 'peersSendingToUs',
                    'rateDownload', 'rateUpload', 'percentDone', 'isFinished',
                    'peersConnected', 'uploadedEver', 'sizeWhenDone', 'status',
                    'webseedsSendingToUs', 'uploadRatio', 'eta',
                    'seedRatioLimit', 'seedRatioMode',
                ],
            },
        };
        if (this._torrents !== false)
            params.arguments.ids = 'recently-active';

        this.sendPost(params, this.processList);
        if (this._timers.list)
            delete this._timers.list;
    }

    retrieveStats() {
        let params = {
            method: 'session-stats',
        };

        this.sendPost(params, this.processStats);
        if (this._timers.stats)
            delete this._timers.stats;
    }

    retrieveSession() {
        let params = {
            method: 'session-get',
        };

        this.sendPost(params, this.processSession);
        if (this._timers.session)
            delete this._timers.session;
    }

    torrentAction(action, torrentId) {
        let params = {
            method: 'torrent-%s'.format(action),
        };
        if (torrentId) {
            params.arguments = {
                ids: [
                    torrentId,
                ],
            };
        }

        this.sendPost(params, this.onTorrentAction);
    }

    torrentAdd(url) {
        let params = {
            method: 'torrent-add',
            arguments: {
                filename: url,
            },
        };

        this.sendPost(params, this.onTorrentAdd);
    }

    setAltSpeed(enable) {
        let params = {
            method: 'session-set',
            arguments: {
                'alt-speed-enabled': enable,
            },
        };

        this.sendPost(params, this.onSessionAction);
    }

    processList(session, result) {
        let message = session.get_async_result_message(result);

        if (message.status_code !== Soup.Status.OK) {
            console.log(`${this._extensionName} - Invalid response to processList: ${message.status_code} = ${message.reason_phrase}`);
            return;
        }

        let decoder = new TextDecoder('utf-8');
        let response = JSON.parse(decoder.decode(session.send_and_read_finish(result).get_data()));
        this._torrents = response.arguments.torrents;

        let toRemove = response.arguments.removed;
        transmissionDaemonIndicator.updateList(toRemove);

        if (!this._timers.list) {
            this._timers.list = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT,
                this._interval, this.retrieveList.bind(this));
        }
    }

    processStats(session, result) {
        let message = session.get_async_result_message(result);

        switch (message.status_code) {
        case Soup.Status.OK: {
            let decoder = new TextDecoder('utf-8');
            let response = JSON.parse(decoder.decode(session.send_and_read_finish(result).get_data()));
            this._stats = response.arguments;
            transmissionDaemonIndicator.updateStats();
            break;
        }
        case Soup.Status.UNAUTHORIZED:
            // See "this.authenticate".
            this.torrents = false;
            break;
        case Soup.Status.NOT_FOUND:
            transmissionDaemonIndicator.connectionError(
                ErrorType.CONNECTION_ERROR, _("Can't access to %s").format(this._url));
            this.torrents = false;
            break;
        case Soup.Status.CONFLICT:
            this._session_id = message.response_headers.get_one('X-Transmission-Session-Id');
            this.retrieveInfos();
            return;
        default:
            transmissionDaemonIndicator.connectionError(
                ErrorType.CONNECTION_ERROR, _("Can't connect to Transmission"));
            this.torrents = false;
            break;
        }

        if (!this._timers.stats) {
            this._timers.stats = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT,
                this._interval, this.retrieveStats.bind(this));
        }
    }

    processSession(session, result) {
        let message = session.get_async_result_message(result);

        if (message.status_code !== Soup.Status.OK) {
            console.log(`${this._extensionName} - Invalid response to processSession: ${message.status_code} = ${message.reason_phrase}`);
            return;
        }

        let decoder = new TextDecoder('utf-8');
        let response = JSON.parse(decoder.decode(session.send_and_read_finish(result).get_data()));
        this._session = response.arguments;

        transmissionDaemonIndicator.toggleTurtleMode(this._session['alt-speed-enabled']);

        // Compatibility with older daemons
        if (this._session['rpc-version'] < 14) {
            TransmissionStatus = {
                CHECK_WAIT: 1,
                CHECK: 2,
                DOWNLOAD: 4,
                SEED: 8,
                STOPPED: 16,
            };
        }

        if (!this._timers.session) {
            this._timers.session = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT,
                this._interval * 1.8, this.retrieveSession.bind(this));
        }
    }

    onSessionAction(session, result) {
        let message = session.get_async_result_message(result);

        if (message.status_code !== Soup.Status.OK) {
            let bytes = session.send_and_read_finish(result);
            let decoder = new TextDecoder('utf-8');
            let response = decoder.decode(bytes.get_data());
            console.log(`${this._extensionName} - Session Action Error: ${response}`);
        }
    }

    onTorrentAction(session, result) {
        let message = session.get_async_result_message(result);

        if (message.status_code !== Soup.Status.OK) {
            let bytes = session.send_and_read_finish(result);
            let decoder = new TextDecoder('utf-8');
            let response = decoder.decode(bytes.get_data());
            console.log(`${this._extensionName} - Torrent Action Error: ${response}`);
        }
    }

    onTorrentAdd(session, result) {
        let response = JSON.parse(result.response_body.data);
        let added = !!response.arguments['torrent-added'];
        transmissionDaemonIndicator.torrentAdded(added);
    }

    getList() {
        return this._torrents;
    }

    getStats() {
        return this._stats;
    }

    getTorrentById(id) {
        for (let i in this._torrents) {
            if (this._torrents[i].id === id)
                return this._torrents[i];
        }

        return null;
    }

    destroy() {
        console.log(`${this._extensionName}: Daemon Monitor - Destroying....bye bye`);
        for (let source in this._timers)
            GLib.source_remove(this._timers[source]);
    }
};

const TransmissionDaemonIndicator = GObject.registerClass({
    GTypeName: 'TransmissionDaemonIndicator',
}, class TransmissionDaemonIndicator extends PanelMenu.Button {
    constructor(extension) {
        super(0.0, `${extension.metadata.name} Indicator`);

        this._extension = extension;
        this._extensionName = this._extension.metadata.name;
        this._extensionDir = this._extension.dir;
        this._settings = this._extension.getSettings();

        this._iconsPath = this._extensionDir.get_child('icons').get_path();

        this._torrents = {};
        this._monitor = transmissionDaemonMonitor;
        this._host = '';
        this._url = '';
        this._server_type = 'daemon';
        this._state = ErrorType.CONNECTING;
        this._nb_torrents = 0;
        this._always_show = false;

        this._stop_btn = new ControlButton('media-playback-pause', _('Pause all torrents'),
            this.stopAll.bind(this));
        this._start_btn = new ControlButton('media-playback-start', _('Start all torrents'),
            this.startAll.bind(this));
        this._web_btn = new ControlButton('web-browser', _('Open Web UI'),
            this.launchWebUI.bind(this));
        this._client_btn = new ControlButton(enabledIcon, _('Open Transmission'),
            this.launchClient.bind(this), null, this._iconsPath);
        this._pref_btn = new ControlButton('preferences-system', _('Preferences'),
            this.launchPrefs.bind(this));
        this._add_btn = new ControlButton('list-add', _('Add torrent'),
            this.toggleAddEntry.bind(this));
        this._turtle_btn = new ControlButton(turtleIcon, _('Toggle turtle mode'),
            this.toggleTurtleMode.bind(this), null, this._iconsPath);
        this._display_btn = new ControlButton('view-list-bullet', _('Toggle display mode'),
            this.toggleDisplayMode.bind(this));

        this._indicatorBox = new St.BoxLayout();

        this._icon = new St.Icon({
            gicon: getCustomIcon(`${connectingIcon}-symbolic`, this._iconsPath),
            style_class: 'system-status-icon',
        });

        this._status = new St.Label({text: ''});
        this._statusBin = new St.Bin({
            child: this._status,
            y_align: Clutter.ActorAlign.CENTER,
        });

        this._indicatorBox.add_child(this._icon);
        this._indicatorBox.add_child(this._statusBin);

        this.add_child(this._indicatorBox);

        let menu = new TransmissionTorrentsMenu(this);
        menu._delegate = this;
        this.setMenu(menu);

        this.updateOptions();
        let settingsId = this._settings.connectObject('changed', () => {
            this.updateOptions();
            this.updateStats(true);
        }, this);

        this.connectObject('destroy', () => {
            this._settings.disconnectObject(settingsId, this);
        });

        this.refreshControls(false);

        if (this._settings.get_enum(TDAEMON_TORRENTS_DISPLAY) ===
            TorrentDisplayClass.TransmissionTorrentMenuSmall)
            this.toggleDisplayMode(true);
    }

    hide() {
        if (!this._always_show && this.visible)
            this.visible = false;
    }

    show() {
        if (!this.visible)
            this.visible = true;
    }

    updateOptions() {
        this._status_show_torrents = this._settings.get_boolean(TDAEMON_STATS_NB_TORRENTS_KEY);
        this._status_show_icons = this._settings.get_boolean(TDAEMON_STATS_ICONS_KEY);
        this._status_show_numeric = this._settings.get_boolean(TDAEMON_STATS_NUMERIC_KEY);
        this._always_show = this._settings.get_boolean(TDAEMON_ALWAYS_SHOW_KEY);
        if (this._always_show)
            this.show();
        else if (this._state === ErrorType.CONNECTION_ERROR)
            this.hide();

        this._host = this._settings.get_string(TDAEMON_HOST_KEY);
        let port = this._settings.get_int(TDAEMON_PORT_KEY);
        let rpcUrl = this._settings.get_string(TDAEMON_RPC_URL_KEY);
        let method = this._settings.get_boolean(TDAEMON_SSL_KEY) ? 'https' : 'http';

        if (method === 'https')
            port = 443;

        this._url = '%s://%s:%s%sweb/'.format(method, this._host, port.toString(), rpcUrl);
    }

    _onOpenStateChanged(menu, open) {
        super._onOpenStateChanged(menu, open);
        if (open)
            this._monitor.changeInterval(2);
        else
            this._monitor.changeInterval(10);
    }

    torrentAdded(added) {
        this.menu.controls.torrentAdded(added);
    }

    connectionError(type, error) {
        if (type === ErrorType.CONNECTION_ERROR)
            this.hide();
        else
            this.show();

        this._state = type;
        this.removeTorrents();

        this._icon.gicon = getCustomIcon(`${errorIcon}-symbolic`, this._iconsPath);
        this._status.text = '';

        this.menu.controls.setInfo(error);
        this.refreshControls(true);
    }

    connectionAvailable() {
        if (this._state !== ErrorType.NO_ERROR) {
            this._icon.gicon = getCustomIcon(`${enabledIcon}-symbolic`, this._iconsPath);
            this._state = ErrorType.NO_ERROR;
            this.checkServer();
            this.show();
        } else {
            this.refreshControls(false);
        }
    }

    checkServer() {
        const DBusIface = '<node>' +
            '<interface name="org.freedesktop.DBus">' +
                '<method name="ListNames">' +
                    '<arg type="as" direction="out" />' +
                '</method>' +
            '</interface></node>';
        const DBusProxy = Gio.DBusProxy.makeProxyWrapper(DBusIface);

        let proxy = new DBusProxy(Gio.DBus.session, 'org.freedesktop.DBus',
            '/org/freedesktop/DBus');

        proxy.ListNamesRemote(names => {
            this._server_type = 'daemon';
            for (let n in names[0]) {
                let name = names[0][n];
                if (name.search('com.transmissionbt.transmission') > -1 &&
                      (this._host === 'localhost' || this._host === '127.0.0.1')) {
                    this._server_type = 'client';
                    break;
                }
            }

            this.refreshControls(true);
        });
    }

    updateStats(dontChangeState) {
        let stats = this._monitor.getStats();
        let statsText = '';
        let infoText = '';

        this._nb_torrents = stats.torrentCount;

        if (this._status_show_torrents && stats.torrentCount > 0)
            statsText += stats.torrentCount;

        if (stats.downloadSpeed > 10000) {
            if (statsText && this._status_show_icons)
                statsText += ' ';

            if (this._status_show_icons)
                statsText += downArrow;

            if (this._status_show_numeric)
                statsText += ' %s/s'.format(readableSize(stats.downloadSpeed));
        }
        if (stats.uploadSpeed > 2000) {
            if (this._status_show_icons && this._status_show_numeric)
                statsText += ' ';

            if (this._status_show_icons)
                statsText += upArrow;

            if (this._status_show_numeric)
                statsText += ' %s/s'.format(readableSize(stats.uploadSpeed));
        }

        if (statsText)
            statsText = ' %s'.format(statsText);

        this._status.text = statsText;

        if (this._nb_torrents > 0) {
            infoText = '%s %s/s / %s %s/s'.format(
                downArrow, readableSize(stats.downloadSpeed), upArrow,
                readableSize(stats.uploadSpeed));
        } else {
            infoText = _('No torrent');
        }

        this.menu.controls.setInfo(infoText);

        if (!dontChangeState)
            this.connectionAvailable();
    }

    refreshControls(stateChanged) {
        if (stateChanged) {
            this.menu.controls.removeControls();
            this.menu.bottom_controls.removeControls();
            this.menu.filters.hide();
        }

        if (this._state === ErrorType.NO_ERROR) {
            if (this._server_type === 'daemon')
                this.menu.controls.addControl(this._web_btn, 0);
            else
                this.menu.controls.addControl(this._client_btn, 0);

            this.menu.controls.addControl(this._add_btn);

            if (this._nb_torrents > 0) {
                this.menu.controls.addControl(this._stop_btn);
                this.menu.controls.addControl(this._start_btn);
                this.menu.filters.show();
            } else {
                this.menu.controls.removeControl(this._stop_btn);
                this.menu.controls.removeControl(this._start_btn);
                this.menu.filters.hide();
            }

            this.menu.bottom_controls.addControl(this._turtle_btn);
            this.menu.bottom_controls.addControl(this._display_btn);
        }

        this.menu.controls.addControl(this._pref_btn);
    }

    stopAll() {
        this._monitor.torrentAction('stop');
    }

    startAll() {
        this._monitor.torrentAction('start');
    }

    launchWebUI() {
        Gio.app_info_launch_default_for_uri(this._url, global.create_app_launch_context(0, -1));
        this.menu.close();
    }

    launchClient() {
        let appSys = Shell.AppSystem.get_default();
        let app = appSys.lookup_app('transmission-gtk.desktop');
        let appWin = this.findAppWindow(app);
        let workspaceManager = global.workspace_manager;
        let workspaceIndex = workspaceManager.get_active_workspace_index();
        let workspace = workspaceManager.get_active_workspace();

        if (app.is_on_workspace(workspace)) {
            if (appWin && global.display.focus_window === appWin) {
                appWin.minimize();
                this.menu.close();
            } else {
                app.activate_full(-1, 0);
            }
        } else {
            if (appWin) {
                appWin.change_workspace_by_index(workspaceIndex, false,
                    global.get_current_time());
            }

            app.activate_full(-1, 0);
        }
    }

    findAppWindow(app) {
        let tracker = Shell.WindowTracker.get_default();
        let windowActors = global.get_window_actors();
        for (let i in windowActors) {
            let win = windowActors[i].get_meta_window();
            if (tracker.get_window_app(win) === app)
                return win;
        }

        return false;
    }

    launchPrefs() {
        this._extension.openPreferences();
        this.menu.close();
    }

    toggleAddEntry() {
        this.menu.controls.toggleAddEntry(this._add_btn);
    }

    toggleTurtleMode(state) {
        this.menu.bottom_controls.toggleTurtleMode(this._turtle_btn, state);
    }

    toggleDisplayMode(state) {
        this.menu.bottom_controls.toggleDisplayMode(this._display_btn, state);
    }

    updateList(toRemove) {
        this.cleanTorrents(toRemove);
        this.updateTorrents();
        this.menu.filters.filterByState();
    }

    cleanTorrents(toRemove) {
        for (let id in toRemove)
            this.removeTorrent(toRemove[id]);
    }

    removeTorrents() {
        for (let id in this._torrents)
            this.removeTorrent(id);
    }

    removeTorrent(id) {
        if (this._torrents[id]) {
            this._torrents[id].destroy();
            delete this._torrents[id];
        }
    }

    updateTorrents() {
        let torrents = this._monitor.getList();
        for (let i in torrents)
            this.updateTorrent(torrents[i]);
    }

    updateTorrent(torrent) {
        if (!this._torrents[torrent.id])
            this.addTorrent(torrent);
        else
            this._torrents[torrent.id].update(torrent);
    }

    addTorrent(torrent, visible) {
        let DisplayClass = TransmissionTorrentDisplayClasses[
            this._settings.get_enum(TDAEMON_TORRENTS_DISPLAY)];
        this._torrents[torrent.id] = new DisplayClass(torrent);
        if (visible === false)
            this._torrents[torrent.id].hide();

        this.menu.addMenuItem(this._torrents[torrent.id]);
    }

    changeTorrentClass() {
        for (let id in this._torrents) {
            let visible = this._torrents[id].actor.visible;
            let torrent = this._torrents[id]._params;
            this.removeTorrent(id);
            this.addTorrent(torrent, visible);
        }
    }

    toString() {
        return '[object TransmissionDaemonIndicator]';
    }
});

const TransmissionTorrentMenuSmall = GObject.registerClass({
    GTypeName: 'TransmissionTorrentMenuSmall',
}, class TransmissionTorrentMenuSmall extends PopupMenu.PopupBaseMenuItem {
    constructor(params) {
        super({
            style_class: 'torrent-menu-small',
        });

        this.actor.remove_style_class_name('popup-menu-item');
        this.actor.remove_style_class_name('popup-inactive-menu-item');

        this._params = params;
        this._info = '';
        this._error = false;

        this.nameLabel = new St.Label({
            text: this._params.name,
            style_class: 'torrent-name-text-small',
        });
        this.add_child(this.nameLabel);

        this.infoBox = new St.BoxLayout({
            vertical: false,
            style_class: 'torrent-infos-small',
            x_expand: true,
            x_align: Clutter.ActorAlign.END,
        });

        this.ratioLabel = new St.Label({
            text: '',
            style_class: 'torrent-ratio',
        });
        this.infoBox.add_child(this.ratioLabel);

        this.downloadRateLabel = new St.Label({
            text: '',
            style_class: 'torrent-download-rate',
        });
        this.infoBox.add_child(this.downloadRateLabel);

        this.uploadRateLabel = new St.Label({
            text: '',
            style_class: 'torrent-upload-rate',
        });
        this.infoBox.add_child(this.uploadRateLabel);

        this.percentDoneLabel = new St.Label({
            text: '',
            style_class: 'torrent-percent-done',
        });
        this.infoBox.add_child(this.percentDoneLabel);

        this.add_child(this.infoBox);
        this.buildInfo();
    }

    buildInfo() {
        let ratio = _('Ratio %s, '.format(this._params.uploadRatio.toFixed(1)));
        let downloadRate = '%s %s/s, '.format(downArrow, readableSize(this._params.rateDownload));
        let uploadRate = '%s %s/s, '.format(upArrow, readableSize(this._params.rateUpload));
        let percentDone = '%s%'.format((this._params.percentDone * 100).toFixed(1));

        this.ratioLabel.text = this._params.uploadRatio.toFixed(1) > 0 ? ratio : '';
        this.downloadRateLabel.text = this._params.rateDownload > 0 ? downloadRate : '';
        this.uploadRateLabel.text = this._params.rateUpload > 0 ? uploadRate : '';
        this.percentDoneLabel.text = percentDone;

        if (this._params.error)
            this.nameLabel.add_style_class_name('torrent-error-small');
        else
            this.nameLabel.remove_style_class_name('torrent-error-small');
    }

    update(params) {
        this._params = params;
        this.buildInfo();
    }

    toString() {
        return '[object TransmissionTorrentMenuSmall <%s>]'.format(this._params.name);
    }

    close() {}
});

class TransmissionTorrentMenu extends PopupMenu.PopupMenuSection {
    constructor(params) {
        super();

        this._params = params;
        this._infos = {};
        this._error = false;
        this.buildInfo();

        this._name = new TransmissionTorrentName(this._params);
        this._name.actor.remove_style_class_name('popup-menu-item');
        this._name.actor.remove_style_class_name('popup-inactive-menu-item');
        this.addMenuItem(this._name);

        this._seeds_info = new PopupMenu.PopupMenuItem(
            this._infos.seeds, {
                style_class: 'torrent-infos',
            });
        this._seeds_info.actor.remove_style_class_name('popup-menu-item');
        this._seeds_info.actor.remove_style_class_name('popup-inactive-menu-item');
        this.addMenuItem(this._seeds_info);

        this._progress_bar = new St.DrawingArea({
            style_class: 'progress-bar',
        });
        this._progress_bar.height = 10;
        this._progress_bar.connectObject('repaint', this._draw.bind(this), this);
        this.actor.add_child(this._progress_bar);

        this._error_info = new PopupMenu.PopupMenuItem(
            this._infos.error, {
                style_class: 'torrent-infos torrent-error',
            });
        this._error_info.label_actor.add_style_class_name('torrent-error-text');

        this._error_info.actor.remove_style_class_name('popup-menu-item');
        this._error_info.actor.remove_style_class_name('popup-inactive-menu-item');
        this.addMenuItem(this._error_info);
        this._error_info.actor.hide();

        this._size_info = new PopupMenu.PopupMenuItem(
            this._infos.size, {
                style_class: 'torrent-infos size-info',
            });
        this._size_info.actor.remove_style_class_name('popup-menu-item');
        this._size_info.actor.remove_style_class_name('popup-inactive-menu-item');
        this.addMenuItem(this._size_info);

        this.update(this._params);
    }

    getStateString(state) {
        switch (state) {
        case TransmissionStatus.STOPPED:
            if (this._params.isFinished)
                return _('Seeding complete');
            else
                return _('Paused');
        case TransmissionStatus.CHECK_WAIT:
            return _('Queued for verification');
        case TransmissionStatus.CHECK:
            return _('Verifying local data');
        case TransmissionStatus.DOWNLOAD_WAIT:
            return _('Queued for download');
        case TransmissionStatus.DOWNLOAD:
            return _('Downloading');
        case TransmissionStatus.SEED_WAIT:
            return _('Queued for seeding');
        case TransmissionStatus.SEED:
            return _('Seeding');
        }

        return false;
    }

    buildInfo() {
        let rateDownload = readableSize(this._params.rateDownload);
        let rateUpload = readableSize(this._params.rateUpload);
        let currentSize = readableSize(this._params.sizeWhenDone * this._params.percentDone);
        let sizeWhenDone = readableSize(this._params.sizeWhenDone);
        let uploadedEver = readableSize(this._params.uploadedEver);
        let percentDone = '%s%'.format((this._params.percentDone * 100).toFixed(1));
        let eta = this._params.eta;
        this._params.percentUploaded = this._params.uploadedEver / this._params.sizeWhenDone;

        this._infos.seeds = '';
        this._infos.size = '';
        this._infos.error = '';

        switch (this._params.status) {
        case TransmissionStatus.STOPPED:
        case TransmissionStatus.CHECK_WAIT:
        case TransmissionStatus.CHECK:
            this._infos.seeds = this.getStateString(this._params.status);
            if (this._params.isFinished) {
                this._infos.size = _('%s, uploaded %s (Ratio %s)').format(
                    sizeWhenDone, uploadedEver, this._params.uploadRatio.toFixed(1));
            } else {
                this._infos.size = _('%s of %s (%s)').format(currentSize, sizeWhenDone, percentDone);
            }
            break;
        case TransmissionStatus.DOWNLOAD_WAIT:
        case TransmissionStatus.DOWNLOAD:
            if (this._params.status === TransmissionStatus.DOWNLOAD) {
                this._infos.seeds = _('Downloading from %s of %s peers - %s %s/s %s %s/s').format(
                    this._params.peersSendingToUs, this._params.peersConnected, downArrow,
                    rateDownload, upArrow, rateUpload);
            } else {
                this._infos.seeds = this.getStateString(TransmissionStatus.DOWNLOAD_WAIT);
            }

            // Format ETA string
            if (eta < 0 || eta >= 999 * 60 * 60)
                eta = _('Remaining time unknown');
            else
                eta = _('%s remaining').format(timeInterval(eta));

            this._infos.size = _('%s of %s (%s) - %s').format(currentSize, sizeWhenDone,
                percentDone, eta);
            break;
        case TransmissionStatus.SEED_WAIT:
        case TransmissionStatus.SEED:
            if (this._params.status === TransmissionStatus.SEED) {
                this._infos.seeds = _('Seeding to %s of %s peers - %s %s/s').format(
                    this._params.peersGettingFromUs, this._params.peersConnected,
                    upArrow, rateUpload);
            } else {
                this._infos.seeds = this.getStateString(TransmissionStatus.SEED_WAIT);
            }

            this._infos.size = _('%s, uploaded %s (Ratio %s)').format(
                sizeWhenDone, uploadedEver, this._params.uploadRatio.toFixed(1));
            break;
        }

        if (this._params.error && this._params.errorString) {
            switch (this._params.error) {
            case TransmissionError.TRACKER_WARNING:
                this._infos.error = _('Tracker returned a warning: %s').format(this._params.errorString);
                break;
            case TransmissionError.TRACKER_ERROR:
                this._infos.error = _('Tracker returned an error: %s').format(this._params.errorString);
                break;
            case TransmissionError.LOCAL_ERROR:
                this._infos.error = _('Error: %s').format(this._params.errorString);
                break;
            }
            this._error = true;
        } else {
            this._error = false;
        }
    }

    _draw() {
        let themeNode = this._progress_bar.get_theme_node();
        let barHeight = themeNode.get_length('-bar-height');
        let borderWidth = themeNode.get_length('-bar-border-width');
        let barColor = themeNode.get_color('-bar-color');
        let barBorderColor = themeNode.get_color('-bar-border-color');
        let uploadedColor = themeNode.get_color('-uploaded-color');
        let seedColor = themeNode.get_color('-seed-color');
        let seedBorderColor = themeNode.get_color('-seed-border-color');
        let downloadColor = themeNode.get_color('-download-color');
        let downloadBorderColor = themeNode.get_color('-download-border-color');
        let idleColor = themeNode.get_color('-idle-color');
        let idleBorderColor = themeNode.get_color('-idle-border-color');
        let padding = 27;

        this._progress_bar.set_height(barHeight);
        let [width, height] = this._progress_bar.get_surface_size();
        let cr = this._progress_bar.get_context();

        width -= padding;

        let color = barColor;
        let borderColor = barBorderColor;
        // Background
        cr.rectangle(padding, 0, width, height);
        cr.setSourceRGB(color.red / 255, color.green / 255, color.blue / 255);
        cr.fillPreserve();
        cr.setLineWidth(borderWidth);
        cr.setSourceRGB(borderColor.red / 255, borderColor.green / 255, borderColor.blue / 255);
        cr.stroke();

        // Downloaded
        let showUpload = false;
        let widthDownloaded = Math.round(width * this._params.percentDone);

        switch (this._params.status) {
        case TransmissionStatus.STOPPED:
        case TransmissionStatus.CHECK_WAIT:
        case TransmissionStatus.CHECK:
            color = idleColor;
            borderColor = idleBorderColor;
            break;
        case TransmissionStatus.DOWNLOAD_WAIT:
        case TransmissionStatus.DOWNLOAD:
            color = downloadColor;
            borderColor = downloadBorderColor;
            break;
        case TransmissionStatus.SEED_WAIT:
        case TransmissionStatus.SEED:
            color = seedColor;
            borderColor = seedBorderColor;
            showUpload = true;
            break;
        }
        cr.setSourceRGB(color.red / 255, color.green / 255, color.blue / 255);
        cr.rectangle(padding, 0, widthDownloaded, height);
        cr.setSourceRGB(color.red / 255, color.green / 255, color.blue / 255);
        cr.fillPreserve();
        cr.setLineWidth(borderWidth);
        cr.setSourceRGB(borderColor.red, borderColor.green, borderColor.blue);
        cr.stroke();

        // Uploaded
        if (showUpload) {
            let ratio = this._params.uploadRatio;
            if (this._params.seedRatioMode === 1)
                ratio /= this._params.seedRatioLimit;

            if (this._params.seedRatioMode === 0 && transmissionDaemonMonitor._session.seedRatioLimited)
                ratio /= transmissionDaemonMonitor._session.seedRatioLimit;

            if (ratio > 1)
                ratio = 1;

            let widthUploaded = Math.round(width * ratio);
            color = uploadedColor;
            borderColor = seedBorderColor;
            cr.setSourceRGB(color.red / 255, color.green / 255, color.blue / 255);
            cr.rectangle(padding, 0, widthUploaded, height);
            cr.setSourceRGB(color.red / 255, color.green / 255, color.blue / 255);
            cr.fillPreserve();
            cr.setLineWidth(borderWidth);
            cr.setSourceRGB(borderColor.red, borderColor.green, borderColor.blue);
            cr.stroke();
        }
        cr.$dispose();
    }

    update(params) {
        this._params = params;
        this.buildInfo();
        this._seeds_info.label.text = this._infos.seeds;
        if (this._error) {
            this._error_info.label.text = this._infos.error;
            this._error_info.actor.show();
        } else {
            this._error_info.actor.hide();
        }
        this._size_info.label.text = this._infos.size;
        this._progress_bar.queue_repaint();
        this._name.update(this._params);
    }

    toString() {
        return '[object TransmissionTorrentMenu <%s>]'.format(this._params.name);
    }

    hide() {
        this.actor.hide();
    }

    show() {
        this.actor.show();
    }

    destroy() {
        this._name.destroy();
        this._seeds_info.destroy();
        this._progress_bar.destroy();
        this._error_info.destroy();
        this._size_info.destroy();
        super.destroy();
    }
}

const TransmissionTorrentDisplayClasses = [TransmissionTorrentMenu, TransmissionTorrentMenuSmall];

const TransmissionTorrentName = GObject.registerClass({
    GTypeName: 'TransmissionTorrentName',
}, class TransmissionTorrentName extends PopupMenu.PopupBaseMenuItem {
    constructor(params) {
        super({
            style_class: 'torrent-name',
        });

        this.id = params.id;
        this.status = params.status;

        this.box = new St.BoxLayout({
            vertical: false,
            style_class: 'torrent-menu-controls',
            x_expand: true,
            x_align: Clutter.ActorAlign.END,
        });

        let nameLabel = new St.Label({
            text: params.name,
            style_class: 'torrent-name-text',
        });

        this.add_child(nameLabel);
        this.add_child(this.box);

        this.updateButtons();
    }

    start() {
        transmissionDaemonMonitor.torrentAction('start', this.id);
    }

    stop() {
        transmissionDaemonMonitor.torrentAction('stop', this.id);
    }

    remove() {
        transmissionDaemonMonitor.torrentAction('remove', this.id);
    }

    update(params) {
        this.status = params.status;
        this.updateButtons();
    }

    updateButtons() {
        this.box.destroy_all_children();
        let startStopBtn;
        switch (this.status) {
        case TransmissionStatus.STOPPED:
        case TransmissionStatus.CHECK_WAIT:
        case TransmissionStatus.CHECK:
            startStopBtn = new ControlButton('media-playback-start', null,
                this.start.bind(this), 'small');
            break;
        default:
            startStopBtn = new ControlButton('media-playback-pause', null,
                this.stop.bind(this), 'small');
            break;
        }
        let removeBtn = new ControlButton('user-trash', null,
            this.remove.bind(this), 'small');

        this.box.add_child(startStopBtn.actor);
        this.box.add_child(removeBtn.actor);
    }
});

const TransmissionTorrentsControls = GObject.registerClass({
    GTypeName: 'TransmissionTorrentsControls',
}, class TransmissionTorrentsControls extends PopupMenu.PopupBaseMenuItem {
    constructor() {
        super({
            reactive: false,
            style_class: 'torrents-controls',
        });

        this.hide();

        this._old_info = '';
        this.hover = false;

        this.vbox = new St.BoxLayout({
            vertical: true,
            style_class: 'torrents-controls-vbox',
            x_expand: true,
        });

        this.ctrl_box = new St.BoxLayout({
            vertical: false,
            x_expand: true,
        });

        this.ctrl_btns = new St.BoxLayout({
            vertical: false,
            style_class: 'torrents-controls-btn',
        });

        this.ctrl_info = new St.Label({
            style_class: 'torrents-controls-text',
            text: '',
            x_expand: true,
            x_align: Clutter.ActorAlign.END,
        });

        this.ctrl_box.add_child(this.ctrl_btns);
        this.ctrl_box.add_child(this.ctrl_info);

        this.vbox.add_child(this.ctrl_box);

        this.add_child(this.vbox);
    }

    setInfo(text) {
        if (!this.hover)
            this.ctrl_info.text = text;
    }

    addControl(button, position) {
        if (!this.ctrl_btns.contains(button.actor)) {
            if (position)
                this.ctrl_btns.insert_child_at_index(button.actor, position);
            else
                this.ctrl_btns.add_child(button.actor);

            this.show();
            button.actor.connectObject('notify::hover', btn => {
                this.hover = btn.hover;
                if (this.hover) {
                    if (btn._delegate._info !== this.ctrl_info.text)
                        this._old_info = this.ctrl_info.text;

                    this.ctrl_info.text = btn._delegate._info;
                } else {
                    this.ctrl_info.text = this._old_info;
                }
            }, this);
        }
    }

    removeControl(button) {
        let buttonActor = button;
        if (button instanceof ControlButton)
            buttonActor = button.actor;

        if (this.ctrl_btns.contains(buttonActor))
            this.ctrl_btns.remove_child(buttonActor);

        if (this.ctrl_btns.get_children().length === 0)
            this.hide();
    }

    removeControls() {
        this.ctrl_btns.get_children().forEach(b => {
            this.removeControl(b);
        });
        this.hide();
    }
});

const TransmissionTorrentsTopControls = GObject.registerClass({
    GTypeName: 'TransmissionTorrentsTopControls',
}, class TransmissionTorrentsTopControls extends TransmissionTorrentsControls {
    constructor() {
        super({
            reactive: false,
        });

        this.add_box = new St.BoxLayout({
            vertical: false,
            style_class: 'torrents-add',
            x_expand: true,
        });
        this.add_box_btn = false;
        this.add_entry = new St.Entry({
            style_class: 'add-entry',
            hint_text: _('Torrent URL or Magnet link'),
            can_focus: true,
            x_expand: true,
        });
        this.add_btn = new ControlButton('object-select', '', this.torrentAdd.bind(this));
        this.add_box.hide();

        this.add_box.add_child(this.add_entry);
        this.add_box.add_child(this.add_btn.actor);

        // Unicode U+2026 correspond to "..."
        this.ctrl_info.text = _('Connecting\u2026');

        this.vbox.add_child(this.add_box);
    }

    toggleAddEntry(button) {
        this.add_box_btn = button;
        if (this.add_box.visible) {
            this.hideAddEntry();
        } else {
            this.add_box.show();
            //  [minWidth, prefWidth]
            let [, prefWidth] = this.add_entry.get_preferred_width(-1);
            this.add_entry.width = prefWidth;
            this.add_entry.grab_key_focus();
            this.add_box_btn.actor.add_style_pseudo_class('active');
        }
    }

    hideAddEntry() {
        this.add_entry.text = '';
        this.add_entry.remove_style_pseudo_class('error');
        this.add_entry.remove_style_pseudo_class('inactive');
        if (this.add_box_btn)
            this.add_box_btn.actor.remove_style_pseudo_class('active');

        this.add_box.hide();
    }

    torrentAdd() {
        let url = this.add_entry.text;
        if (url.match(/^http/) || url.match(/^magnet:/)) {
            this.add_entry.add_style_pseudo_class('inactive');
            transmissionDaemonMonitor.torrentAdd(url);
        } else {
            this.torrentAdded(false);
        }
    }

    torrentAdded(added) {
        if (added) {
            this.hideAddEntry();
        } else {
            this.add_entry.remove_style_pseudo_class('inactive');
            this.add_entry.add_style_pseudo_class('error');
        }
    }
});

const TransmissionTorrentsBottomControls = GObject.registerClass({
    GTypeName: 'TransmissionTorrentsBottomControls',
}, class TransmissionTorrentsBottomControls extends TransmissionTorrentsControls {
    constructor(extensionSettings) {
        super({
            reactive: false,
        });

        this._settings = extensionSettings;
        this._turtle_state = false;
        this._display_state = false;
    }

    toggleTurtleMode(button, state) {
        if (state === true || state === false) {
            this._turtle_state = state;
        } else {
            this._turtle_state = !this._turtle_state;
            transmissionDaemonMonitor.setAltSpeed(this._turtle_state);
        }

        if (this._turtle_state)
            button.actor.add_style_pseudo_class('active');
        else
            button.actor.remove_style_pseudo_class('active');
    }

    toggleDisplayMode(button, state) {
        if (state === true || state === false)
            this._display_state = state;
        else
            this._display_state = !this._display_state;

        if (this._display_state) {
            button.actor.add_style_pseudo_class('active');
            this._settings.set_enum(TDAEMON_TORRENTS_DISPLAY,
                TorrentDisplayClass.TransmissionTorrentMenuSmall);
        } else {
            button.actor.remove_style_pseudo_class('active');
            this._settings.set_enum(TDAEMON_TORRENTS_DISPLAY,
                TorrentDisplayClass.TransmissionTorrentMenu);
        }

        if (state !== true && state !== false) {
            let indicator = this._delegate._delegate;
            indicator.changeTorrentClass();
        }
    }
});

class ControlButton {
    constructor(icon, info, callback, type, iconPath) {
        let iconSize = 20;
        let padding = 8;
        if (type && type === 'small') {
            iconSize = 16;
            padding = 3;
        }

        this.icon = new St.Icon({
            icon_name: '%s-symbolic'.format(icon),
            icon_size: iconSize,
        });

        if (icon === turtleIcon || icon === enabledIcon)
            this.icon.gicon = getCustomIcon(this.icon.icon_name, iconPath);

        this.actor = new St.Button({
            style_class: 'modal-dialog-button button',
            child: this.icon,
        });
        this.actor._delegate = this;
        this.actor.connectObject('clicked', callback, this);

        // override base style
        this.icon.set_style('padding: 0px');
        this.actor.set_style('padding: %spx'.format(padding.toString()));

        this._info = info;
    }
}

const TransmissionTorrentsFilter = GObject.registerClass({
    GTypeName: 'TransmissionTorrentsFilter',
}, class TransmissionTorrentsFilter extends PopupMenu.PopupMenuItem {
    constructor(stateId) {
        super(StatusFilterLabels[stateId]);
        this.stateId = stateId;
    }

    activate() {
        this._delegate.filterByState(this.stateId);
        this._delegate.menu.close();
    }
});

const TransmissionTorrentsFilters = GObject.registerClass({
    GTypeName: 'TransmissionTorrentsFilters',
}, class TransmissionTorrentsFilters extends PopupMenu.PopupSubMenuMenuItem {
    constructor(extensionSettings) {
        super(StatusFilterLabels[extensionSettings.get_int(TDAEMON_LATEST_FILTER)]);

        this._settings = extensionSettings;
        this.stateId = this._settings.get_int(TDAEMON_LATEST_FILTER);

        for (let state in StatusFilter) {
            let item = new TransmissionTorrentsFilter(StatusFilter[state]);
            item._delegate = this;
            this.menu.addMenuItem(item);
        }
    }

    filterByState(stateId) {
        if (!stateId && stateId !== 0)
            stateId = this.stateId;

        for (let id in transmissionDaemonIndicator._torrents) {
            let torrent = transmissionDaemonIndicator._torrents[id];
            switch (stateId) {
            case StatusFilter.ALL:
                torrent.show();
                break;
            case StatusFilter.ACTIVE:
                if (torrent._params.peersGettingFromUs > 0 || torrent._params.peersSendingToUs > 0 ||
                    torrent._params.webseedsSendingToUs > 0 ||
                    torrent._params.status === TransmissionStatus.CHECK)
                    torrent.show();
                else
                    torrent.hide();
                break;
            case StatusFilter.DOWNLOADING:
                if (torrent._params.status === TransmissionStatus.DOWNLOAD)
                    torrent.show();
                else
                    torrent.hide();
                break;
            case StatusFilter.SEEDING:
                if (torrent._params.status === TransmissionStatus.SEED)
                    torrent.show();
                else
                    torrent.hide();
                break;
            case StatusFilter.PAUSED:
                if (torrent._params.status === TransmissionStatus.STOPPED &&
                    !torrent._params.isFinished)
                    torrent.show();
                else
                    torrent.hide();

                break;
            case StatusFilter.FINISHED:
                if (torrent._params.status === TransmissionStatus.STOPPED &&
                    torrent._params.isFinished)
                    torrent.show();
                else
                    torrent.hide();
                break;
            }
        }
        this._settings.set_int(TDAEMON_LATEST_FILTER, stateId);
        this.stateId = stateId;
        this.label.text = _(StatusFilterLabels[stateId]);
    }
});

class TransmissionTorrentsMenu extends PopupMenu.PopupMenu {
    constructor(sourceActor) {
        super(sourceActor, 0.0, St.Side.TOP);

        this.controls = new TransmissionTorrentsTopControls();
        this.filters = new TransmissionTorrentsFilters(sourceActor._settings);
        this.filters.hide();
        this.bottom_controls = new TransmissionTorrentsBottomControls(sourceActor._settings);
        this.bottom_controls._delegate = this;

        this._scroll = new St.ScrollView({
            style_class: 'vfade popup-sub-menu',
            hscrollbar_policy: St.PolicyType.NEVER,
            vscrollbar_policy: St.PolicyType.AUTOMATIC,
        });

        this._scrollBox = new St.BoxLayout({vertical: true});
        // For compatibility with GS 45 where add_child don't work
        if (this._scroll.add_actor === undefined)
            this._scroll.add_child(this._scrollBox);
        else
            this._scroll.add_actor(this._scrollBox);

        this.addMenuItem(this.controls);
        this.addMenuItem(this.filters);
        this.box.add_child(this._scroll);
        this.addMenuItem(this.bottom_controls);

        let vscroll = this._scroll.get_vscroll_bar();
        vscroll.connectObject('scroll-start', () => {
            this.passEvents = true;
        }, this);
        vscroll.connectObject('scroll-stop', () => {
            this.passEvents = false;
        }, this);
    }

    addMenuItem(menuItem, position) {
        if (menuItem instanceof TransmissionTorrentMenu ||
            menuItem instanceof TransmissionTorrentMenuSmall) {
            this._scrollBox.add_child(menuItem.actor);
            menuItem._closingId = this.connectObject('open-state-changed', (self, open) => {
                if (!open)
                    menuItem.close(false);
            }, this);

            menuItem.connectObject('destroy', () => {
                this.length -= 1;
            }, this);
        } else {
            super.addMenuItem(menuItem, position);
        }
    }

    close(animate) {
        super.close(animate);
        this.controls.hideAddEntry();
    }
}

/**
 * This function convert a size in byte in a more uman readable format, power of 1000,
 * with size unit suffix.
 *
 * @param {number} size - Size in byte.
 * @returns {string} - Return a string containing a formatted size with unit suffix.
 */
function readableSize(size) {
    if (!size)
        size = 0;

    let units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB'];
    let i = 0;
    while (size >= 1000) {
        size /= 1000;
        i += 1;
    }
    let n = i;
    if (n > 0 && size > 0)
        n -= 1;

    return '%s %s'.format(size.toFixed(n), units[i]);
}

/**
 * This function convert an interval in sec in a more uman readable format,
 * with "day, hour minute , second" suffix or a combination of two.
 *
 * @param {number} secs - Interval in seconds.
 * @returns {string} - Return a string containing a formatted interval with unit suffix.
 */
function timeInterval(secs) {
    const days = Math.floor(secs / 86400),
        hours = Math.floor(secs % 86400 / 3600),
        minutes = Math.floor(secs % 3600 / 60),
        seconds = Math.floor(secs % 60),
        d = `${days} ${days > 1 ? _('days') : _('day')}`,
        h = `${hours} ${hours > 1 ? _('hours') : _('hour')}`,
        m = `${minutes} ${minutes > 1 ? _('minutes') : _('minute')}`,
        s = `${seconds} ${seconds > 1 ? _('seconds') : _('second')}`;

    if (days) {
        if (days >= 4 || !hours)
            return d;

        return `${d} ${h}`;
    }

    if (hours) {
        if (hours >= 4 || !minutes)
            return h;

        return `${h} ${m}`;
    }

    if (minutes) {
        if (minutes >= 4 || !seconds)
            return m;

        return `${m} ${s}`;
    }
    return s;
}

/**
 * This function create a new Gio.icon for a custom icon in extension folder.
 *
 * @param {string} iconName - The name of the icon.
 * @param {string} iconPath - The path of the icon.
 * @returns {Gio.icon} - Return a Gio.icon.
 */
function getCustomIcon(iconName, iconPath) {
    return Gio.icon_new_for_string(`${iconPath}/${iconName}.svg`);
}

export default class TransmissionDaemonExtension extends Extension {
    constructor(metadata) {
        super(metadata);

        StatusFilterLabels = {
            0: this.gettext('All'),
            1: this.gettext('Active'),
            2: this.gettext('Downloading'),
            3: this.gettext('Seeding'),
            4: this.gettext('Paused'),
            5: this.gettext('Stopped'),
        };
    }

    enable() {
        console.log(`Enabling ${this.metadata.name} - Version ${this.metadata['version-name']}`);

        transmissionDaemonMonitor = new TransmissionDaemonMonitor(this);
        transmissionDaemonIndicator = new TransmissionDaemonIndicator(this);

        Main.panel.addToStatusArea(`${this.metadata.name} Indicator`, transmissionDaemonIndicator);
    }

    disable() {
        console.log(`Disabling ${this.metadata.name} - Version ${this.metadata['version-name']}`);

        if (transmissionDaemonIndicator !== null) {
            transmissionDaemonIndicator.destroy();
            transmissionDaemonIndicator = null;
        }

        if (transmissionDaemonMonitor !== null) {
            transmissionDaemonMonitor.destroy();
            transmissionDaemonMonitor = null;
        }
    }
}


