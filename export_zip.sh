#!/bin/bash

srcdir=`dirname $0`
srcdir=`(cd $srcdir && pwd)`

builddir=`mktemp -p $srcdir -d _build.XXXXXX` || exit 1
installdir=`mktemp -p $srcdir -d _install.XXXXXX` || exit 1

meson setup --prefix=$installdir $srcdir $builddir
meson install -C $builddir

if [ ! -d $srcdir/zip-files ]; then
    mkdir $srcdir/zip-files
fi

extensiondir=$installdir/share/gnome-shell/extensions/transmission-daemon-ng@glerro.pm.me/

sources=(icons locale `(ls $extensiondir/*.js)` `(ls $extensiondir/*.ui)`)

gnome-extensions pack --force --out-dir=$srcdir/zip-files ${sources[@]/#/--extra-source=} $extensiondir

rm -rf $builddir
rm -rf $installdir

